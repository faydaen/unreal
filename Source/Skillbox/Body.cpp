#include "Body.h"

ABody::ABody()
{
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh->SetCollisionResponseToChannels(ECR_Overlap);
	RootComponent = Mesh; 
}

void ABody::BeginPlay()
{
	Super::BeginPlay();
}

void ABody::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABody::Interact(AActor* Interactor)
{
	OnBite.Broadcast();
}