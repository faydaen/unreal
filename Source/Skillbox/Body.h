#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Body.generated.h"

class UStaticMeshComponent;

UCLASS()
class SKILLBOX_API ABody : public AActor, public IInteractable 
{
	GENERATED_BODY()

public:
	ABody();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	UFUNCTION()
	void Interact(AActor* Interactor) override;

	// событие 
	DECLARE_MULTICAST_DELEGATE(FOnInteractDelegate);
	FOnInteractDelegate OnBite;
	
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};
