#include "Egg.h"

AEgg::AEgg()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AEgg::BeginPlay()
{
	Super::BeginPlay();
}

void AEgg::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEgg::Interact_Implementation(AActor* Interactor)
{
	OnInteract.Broadcast();
}
