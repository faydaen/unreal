#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Egg.generated.h"

UCLASS()
class SKILLBOX_API AEgg : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AEgg();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintNativeEvent)
	void Interact(AActor* Interactor) override;

	// событие 
	DECLARE_MULTICAST_DELEGATE(FOnInteractDelegate);
	FOnInteractDelegate OnInteract;
};
