#include "Game.h"
#include "Egg.h"
#include "Snake.h"
#include "Engine/Classes/Camera/CameraComponent.h"

AGame::AGame()
{
	// создаём камеру
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
}

void AGame::BeginPlay()
{
	Super::BeginPlay();

	// выставляем камеру на 90 градусов
	SetActorRotation(FRotator(-90, 0, 0));

	// спауним змейку
	SpawnSnake();

	// спауним яицо
	SpawnEgg();
}

void AGame::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGame::SpawnSnake()
{
	Snake = GetWorld()->SpawnActor<ASnake>(SnakeClass, FTransform());
	Snake->CreateHead();
	Snake->GrowUp();
	Snake->GrowUp();
	Snake->OnBiteSelf.AddUObject(this, &AGame::OnBiteSelfHandler);
}

void AGame::SpawnEgg()
{
	// все незанятые змейкой клетки
	TArray<FVector> freeCells = Snake->GetFreeCells();

	// получаем случайный индекс из этого массива 
	int32 i = FMath::RandRange(0, freeCells.Num() - 1); 
	Egg = GetWorld()->SpawnActor<AEgg>(EggClass, FTransform(freeCells[i]));	
	Egg->OnInteract.AddUObject(this, &AGame::OnEatenHandler);
}

void AGame::OnEatenHandler()
{
	Egg->Destroy();
	Snake->GrowUp();
	SpawnEgg();
}

void AGame::OnBiteSelfHandler()
{
	// запускаем событие (подписаись на него в блюпринте)
	OnLose.Broadcast();
}

// Событие перехвата ввода
void AGame::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &AGame::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &AGame::HandlePlayerHorizontalInput);
}

void AGame::HandlePlayerVerticalInput(float value)
{
	if (IsValid(Snake))
	{
		Snake->OnVerticalInput(value);
	}
}

void AGame::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(Snake))
	{
		Snake->OnHorizontalInput(value);
	}
}
