#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Game.generated.h"

class UCameraComponent;
class AEgg;
class ASnake;

UCLASS()
class SKILLBOX_API AGame : public APawn
{
	GENERATED_BODY()

public:
	AGame();
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnake> SnakeClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AEgg> EggClass;

	UPROPERTY(BlueprintReadWrite)
	ASnake* Snake;
	
	// количество клеток от центра до правой или левой границы 
	static constexpr int VerticalCellsToBorder = 3;

	// количество клеток от центра до верхней или нижней границы
	static constexpr int HorizontalCellsToBorder =  4;

	// чтобы можно было подписатся на событие в блюпринте
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLoseDelegate);

	UPROPERTY(BlueprintAssignable)
	FLoseDelegate OnLose;

	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void SpawnSnake();

	UFUNCTION()
	virtual void SpawnEgg();
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void OnEatenHandler();

	UFUNCTION()
	void OnBiteSelfHandler();

private:
	UCameraComponent* Camera;

	AEgg* Egg;
};
