#include "Head.h"

#include "Interactable.h"

AHead::AHead()
{
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head"));
	// режим коллизии - объекты будут проходить сквозь него, но у нас будет возможность отследить это взаимодействие
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Mesh->SetCollisionResponseToChannels(ECR_Overlap);
	RootComponent = Mesh;
}

void AHead::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &AHead::HandleBeginOverlap);
}

void AHead::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHead::HandleBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (IInteractable* Interactable = Cast<IInteractable>(OtherActor))
	{
		Interactable->Interact(OverlappedActor);
	}
}
