#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Head.generated.h"

class UStaticMeshComponent;

UCLASS()
class SKILLBOX_API AHead : public AActor
{
	GENERATED_BODY()
	
public:	
	AHead();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void HandleBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
};
