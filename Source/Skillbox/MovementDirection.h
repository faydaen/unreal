﻿#pragma once

UENUM(BlueprintType)
enum class EMovementDirection : uint8
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
};
