#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillboxGameModeBase.generated.h"

UCLASS()
class SKILLBOX_API ASkillboxGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
