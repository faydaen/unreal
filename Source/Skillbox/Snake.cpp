#include "Snake.h"

#include "Body.h"
#include "Head.h"
#include "Game.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

ASnake::ASnake()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(Speed);
}

void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnake::CreateHead()
{
	Head = GetWorld()->SpawnActor<AHead>(HeadClass, FTransform(FVector(0, 0, 0)));
	Head->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
}

void ASnake::GrowUp()
{
	FVector position = Offset() * -1 * (Body.Num() + 1);
	FTransform transform = FTransform(position);
	ABody* BodyPart = GetWorld()->SpawnActor<ABody>(BodyClass, transform);

	BodyPart->OnBite.AddUObject(this, &ASnake::OnBiteBodyHandler);
	
	BodyPart->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	Body.Add(BodyPart);
}

// хендлер укуса тела, тригирит события укуса всей змейки
void ASnake::OnBiteBodyHandler()
{
	OnBiteSelf.Broadcast();
}

void ASnake::Clear()
{
	Head->Destroy();
	for (int i = 0; i < Body.Num(); i++)
	{
		Body[i]->Destroy();
	}
}


void ASnake::Move()
{
	FVector prevLocation = Head->GetActorLocation();
	Head->AddActorWorldOffset(Offset());

	for (int i = 0; i < Body.Num(); i++)
	{
		FVector currentLocation = Body[i]->GetActorLocation();
		Body[i]->SetActorLocation(prevLocation);
		prevLocation = currentLocation;
	}
}

TArray<FVector> ASnake::GetOccupiedCells()
{
	TArray<FVector> occupiedCells;

	occupiedCells.Add(Head->GetActorLocation());
	for (int i = 0; i < Body.Num(); i++)
	{
		occupiedCells.Add(Body[i]->GetActorLocation());
	}

	return occupiedCells;
}

TArray<FVector> ASnake::GetFreeCells()
{
	TArray<FVector> freeCells;
	TArray<FVector> occupiedCells = GetOccupiedCells();

	for (int x = -AGame::VerticalCellsToBorder; x <= AGame::VerticalCellsToBorder; x++)
	{
		for (int y = -AGame::HorizontalCellsToBorder; y <= AGame::HorizontalCellsToBorder; y++)
		{
			if (CellIsFree(occupiedCells, x, y))
			{
				freeCells.Add(FVector(x * CellSize, y * CellSize, 0));
			}
		}
	}

	return freeCells;
}



bool ASnake::CellIsFree(TArray<FVector>& OccupiedCells, int x, int y)
{
	const float quoterOfCell = CellSize / 4;
	const float X = x * CellSize;
	const float Y = y * CellSize;

	for (int i = 0; i < OccupiedCells.Num(); i++)
	{
		// чтобы не было бага связанным с float, проверяем не точное значение точки, а её небольшую окрестность
		bool inX = X - quoterOfCell < OccupiedCells[i].X && OccupiedCells[i].X < X + quoterOfCell;
		bool inY = Y - quoterOfCell < OccupiedCells[i].Y && OccupiedCells[i].Y < Y + quoterOfCell;

		if (inX && inY)
		{
			return false;
		}
	}

	return true;
}

FVector ASnake::Offset()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += CellSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= CellSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += CellSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= CellSize;
		break;
	}

	return MovementVector;
}

void ASnake::OnVerticalInput(float value)
{
	if (value > 0 && LastMoveDirection != EMovementDirection::DOWN)
	{
		LastMoveDirection = EMovementDirection::UP;
	}
	else if (value < 0 && LastMoveDirection != EMovementDirection::UP)
	{
		LastMoveDirection = EMovementDirection::DOWN;
	}
}

void ASnake::OnHorizontalInput(float value)
{
	if (value > 0 && LastMoveDirection != EMovementDirection::LEFT)
	{
		LastMoveDirection = EMovementDirection::RIGHT;
	}
	else if (value < 0 && LastMoveDirection != EMovementDirection::RIGHT)
	{
		LastMoveDirection = EMovementDirection::LEFT;
	}
}
