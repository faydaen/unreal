#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovementDirection.h"
#include "Snake.generated.h"

class AHead;
class ABody;

UCLASS()
class SKILLBOX_API ASnake : public AActor
{
	GENERATED_BODY()

public:
	ASnake();

	UPROPERTY()
	AHead* Head;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AHead> HeadClass;

	UPROPERTY()
	TArray<ABody*> Body;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABody> BodyClass;

	UPROPERTY(EditDefaultsOnly)
	float Speed = 1.0f;

	UPROPERTY(EditDefaultsOnly)
	int CellSize = 100;

	UPROPERTY(EditDefaultsOnly)
	EMovementDirection LastMoveDirection = EMovementDirection::DOWN;

	DECLARE_MULTICAST_DELEGATE(FOnInteractDelegate);

	FOnInteractDelegate OnBiteSelf;


	

	
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void CreateHead();

	UFUNCTION()
	void GrowUp();

	UFUNCTION()
	void Move();
	
	UFUNCTION()
	FVector Offset();

	UFUNCTION()
	void OnVerticalInput(float value);

	UFUNCTION()
	void OnHorizontalInput(float value);

	UFUNCTION()
	TArray<FVector> GetFreeCells();

	UFUNCTION()
	void OnBiteBodyHandler();
	
	UFUNCTION()
	TArray<FVector> GetOccupiedCells();
	
	UFUNCTION()
	bool CellIsFree(TArray<FVector> &OccupiedCells, int x, int y);

	UFUNCTION(BlueprintCallable)
	void Clear();
};
