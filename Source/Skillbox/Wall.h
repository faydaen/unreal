#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Wall.generated.h"

UCLASS()
class SKILLBOX_API AWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AWall();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
	void Interact(AActor* Interactor) override;
};
